# ametro-maps

Content repository with maps for ametro android app

Data is licensed under [pMetro license](https://pmetro.su/License.html) [RU](PMETRO_LICENSE_RU)[EN](PMETRO_LICENSE_EN) and free for use in non-commetrial or GPL projects.
